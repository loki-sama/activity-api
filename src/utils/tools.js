
export default {
    /**
     * Creates an object of obejects with targetKey as keys
     * @param {*} array 
     * @param {*} targetKey 
     */
    arrayToHashMap: function (array, targetKey = '_id') {
        return array.reduce(
            (output, item) => {
                output[item[targetKey]] = item
                return output
            }, {}
        )
    }
}
import Category from '../models/children/Category'
import Activities from '../models/Activities'
import mongoose from "mongoose";
import wiki from 'wikijs';
import parseErrors from "../utils/parseErrors";
import listHobbies from "../lists/listHobbies";
import List_of_academic_fields from "../lists/List_of_academic_fields";
import List_of_sports from "../lists/List_of_sports";
import Outline_of_academic_disciplines from "../lists/Outline_of_academic_disciplines";
import List_of_corporate_titles from "../lists/List_of_corporate_titles";
import List_of_industrial_occupations from "../lists/List_of_industrial_occupations";
import List_of_metalworking_occupations from "../lists/List_of_metalworking_occupations";
import artistJobListWiki from "../lists/artistJobListWiki";
import danceJobListWiki from "../lists/danceJobListWiki";
import List_of_tagged_degrees from "../lists/List_of_tagged_degrees";

import fs from 'fs'
import express from "express";
const router = express.Router();
import wikipedia from "../apis/wikipedia";
import axios from 'axios'
function insertCategories() {
    [
        'Studies / Educatuio / Apprenticeship',    //https://en.wikipedia.org/wiki/List_of_academic_fields
        //https://en.wikipedia.org/wiki/Outline_of_academic_disciplines    
        'Sport',
        'Pastime',
        'Movies',
        'Hobbies', //https://en.wikipedia.org/wiki/List_of_hobbies

        //https://en.wikipedia.org/wiki/Lists_of_occupations
        'Jobs', //https://dot-job-descriptions.careerplanner.com/
        'Games',
        'Musik',
        'Books',
        'Sites',
        'Travels',
        'Products',
        'Food',
        'Programming'
    ].forEach((value, index, result) => {
        new Category({ category: value }).save()
    })
}

function scheduleRequests(axiosInstance, intervalMs) {
    let lastInvocationTime = undefined;

    const scheduler = (config) => {
        const now = Date.now();
        if (lastInvocationTime) {
            lastInvocationTime += intervalMs;
            const waitPeriodForThisRequest = lastInvocationTime - now;
            if (waitPeriodForThisRequest > 0) {
                return new Promise((resolve) => {
                    setTimeout(
                        () => resolve(config),
                        waitPeriodForThisRequest);
                });
            }
        }

        lastInvocationTime = now;
        return config;
    }

    axiosInstance.interceptors.request.use(scheduler);
}

function getActivity(title, array) {
    console.log('====================================');
    console.log("Looking for " + title + " in wikipedia");
    console.log('====================================');
    axios.all(
        [wikipedia.getTitleImage(title),
        wikipedia.getImagesAndInfo(title),
        wikipedia.getDescription(title)])
        .then(axios.spread(function (mainImage, imagesAndUrls, description) {

            let key = Object.keys(mainImage.data.query.pages)[0]
            console.log('====================================');
            console.log(mainImage.data.query);

            console.log('====================================');
            if (!!mainImage.data.query.pages[key].pageimage) {
                let titleImage = mainImage.data.query.pages[key].pageimage.replace(/ /g, "_")
                let url = ""
                let UsageTerms = ""

                for (let x in imagesAndUrls.data.query.pages) {
                    let searchImage = imagesAndUrls.data.query.pages[x].title.replace(/ /g, "_")

                    if (searchImage == 'File:' + titleImage) {
                        url = imagesAndUrls.data.query.pages[x].imageinfo[0].url
                        // UsageTerms = imagesAndUrls.data.query.pages[x].imageinfo[0].extmetadata.UsageTerms.value
                        break;
                    }
                }
                console.log('====================================');
                console.log("Found: ");
                console.log({
                    imageUrl: url,
                    keyImage: key,
                    titleImage: titleImage,
                    name: title,
                });
                console.log('====================================');
                if (url != "") {
                    let key2 = Object.keys(description.data.query.pages)
                    new Activities({
                        image: url,
                        name: title,
                        category: mongoose.Types.ObjectId('59e07913b74d483ef01b21c9'),
                        description: description.data.query.pages[key2].extract
                    }).save().catch((error) => {
                        console.log(error);

                    }

                        )
                }
            }
            getActivitySearch(array)
        }
        )
        )
        .catch((error) => {
            console.log('====================================');
            console.log(error);
            console.log('====================================');
        }
        )
}
function getActivitySearch(array) {
    let value = array.pop()
    if (value) {
        wikipedia.search(value).then(
            (searchResult) => {
                console.log('====================================');
                console.log(value);
                console.log('====================================');
                console.log('====================================');
                console.log(searchResult.data.query.search);
                console.log('====================================');
                let lowerValue = value.toLowerCase().replace(/ /g, "_")
                for (let index in searchResult.data.query.search) {
                    let searchValue = searchResult.data.query.search[index]
                    console.log('====================================');
                    console.log(searchValue);
                    console.log('====================================');
                    if (searchValue.title.toLowerCase().replace(/ /g, "_").includes(lowerValue)
                    ) {
                        return getActivity(searchValue.title, array)
                    }
                }


                return getActivitySearch(array)

            }).catch(
            error => {
                console.log(error);

            }
            )
    }
}



function wikipediaData(pageName) {
    return wiki().page(pageName)
        .then(page => {
            return Promise.all([page.mainImage(), page.images(), page.summary()]).then(
                values => {
                    return {
                        name: pageName,
                        mainImage: values[0],
                        images: values[1],
                        description: values[2],
                    }
                }
            )
        }
        )

}
function wikipediaToMongo(pagenameList, categoryId) {
    if (!!pagenameList && pagenameList != [] && pagenameList != undefined) {
        let pagename = pagenameList.shift()
        console.log("Getting " + pagename);
        wikipediaData(pagename).then(wikipediaData => {
            //add to mongo
            new Activities({
                image: wikipediaData.mainImage,
                name: wikipediaData.name,
                category: mongoose.Types.ObjectId(categoryId),
                description: wikipediaData.description
            }).save().catch(error => (error))
            //get next
            wikipediaToMongo(pagenameList)
        }
        )
            .catch(error => {
                console.log(error)
                wikipediaToMongo(pagenameList)
            }
            );
    }
    else {
        return 'finish'
    }

}


function saveLinks(name) {
    wiki().page(name)
        .then(page => {
            return page.links().then(
                values => {
                console.log('====================================');
                console.log(values);
                console.log('====================================');
                    fs.writeFile(name + '.js', 'export default ' + JSON.stringify(values), (err) => {
                        // throws an error, you could also catch it here
                        if (err) throw err;

                        // success case, the file was saved
                        console.log("saved "+name);
                    });
                }
            )
        }
        )
        .catch(err => {
            console.log('====================================');
            console.log();
            console.log('====================================');
        }
        )
}
//https://en.wikipedia.org/wiki/Lists_of_games
router.get("/", (req, res) => {
    //getActivitySearch(jobList2)
    // wikipediaToMongo(List_of_sports, "59e07913b74d483ef01b21c5") //Sport

    // wikipediaToMongo(listHobbies, "59e07913b74d483ef01b21c8") //Hobbies

    // wikipediaToMongo(List_of_academic_fields, "59e07913b74d483ef01b21cc") //Uni
    // wikipediaToMongo(Outline_of_academic_disciplines, "59e07913b74d483ef01b21cc") //Uni
    // wikipediaToMongo(List_of_tagged_degrees, "59e07913b74d483ef01b21cc") //Uni

    // wikipediaToMongo(danceJobListWiki, "59e07913b74d483ef01b21c9") //Job
    // wikipediaToMongo(artistJobListWiki, "59e07913b74d483ef01b21c9") //Job
    // wikipediaToMongo(List_of_metalworking_occupations, "59e07913b74d483ef01b21c9") //Job
    // wikipediaToMongo(List_of_industrial_occupations, "59e07913b74d483ef01b21c9") //Job
    // wikipediaToMongo(List_of_corporate_titles, "59e07913b74d483ef01b21c9") //Job



    //saveLinks('List_of_board_games')
    // saveLinks('List_of_abstract_strategy_games')
    // saveLinks('List_of_trick-taking_games')
    // saveLinks('Category:Matching_card_games')
    // saveLinks('Category:Shedding-type_card_games')
    // saveLinks('Category:Fishing_card_games')
    // saveLinks('Category:Comparing_card_games')
    // saveLinks('Category:Solitaire_card_games')
    // saveLinks('Category:Drinking_card_games')
    // saveLinks('List_of_collectible_card_games')
    // saveLinks('List_of_poker_variants')
    // saveLinks('Category:Dedicated deck card games')
    // saveLinks('List_of_fictional_games')
    // saveLinks('List_of_dice_games')
    // saveLinks('Category:Party_games')
//  saveLinks('List_of_role-playing_games_by_genre')
    //saveLinks('2010_films')
}
)
//         (searchResult) => {
//             wikipedia.getTitleImage(searchResult.data.query.search[0].title)
//                 .then((imageResult) => {
//                     let key = Object.keys(imageResult.data.query.pages)[0]
//                     return wikipedia.getDescription(searchResult.data.query.search[0].title)
//                         .then(
//                         res.json({
//                             url: imageResult.data.query.pages[key].pageimage,
//                             title: searchResult.data.query.search[0].title,
//                             description: descriptionResult.query.pages[key2].extract

//                         })
//                         )
//                         .catch((error2) => (res.json(error2)))
//                 }
//                 )
//                 .catch(erro3 => {
//                     return res.json(erro3)
//                 })
//         }
//     )
//         .catch(error => res.json(error))
// }
// )

export default router;

const jobList = [
    'STEREO-PLOTTER OPERATOR',
    'STEREOTYPER',
    'STEREOTYPER APPRENTICE',
    'STERILE-PRODUCTS PROCESSOR',
    'STERILIZER',
    'STERILIZER',
    'STERILIZER OPERATOR',
    'STEVEDORE I',
    'STEVEDORE II',]



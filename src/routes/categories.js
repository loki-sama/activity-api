import express from "express";
const router = express.Router();
import Category from '../models/children/Category'

import parseErrors from "../utils/parseErrors";
import authenticate from "../middlewares/authenticate";
import tools from '../utils/tools'
//router.use(authenticate);


/**
 * Search activities images from pixabay
 */
router.get("/", (req, res) => {
    return Category.find({})
        .then((result) => {
            return res.json(tools.arrayToHashMap(result))
        })
        .catch((error) => {
            console.log(error)
            res.status(400).json(error)
        })
});

export default router;
import express from "express";
const router = express.Router();
import File from '../models/children/File'
import parseErrors from "../utils/parseErrors";
import authenticate from "../middlewares/authenticate";
import { search as pixabaySearch } from '../apis/pixabay'
import wiki from 'wikijs';
import multer from 'multer';
import mongoose from "mongoose";

router.use(authenticate);


/**
 * Search activities images from pixabay
 */
router.get("/search/:searchString", (req, res) => {
    return pixabaySearch(req.params.searchString)
        .then((result) => {
            return res.json(result.data)
        })
        .catch((error) => {
            console.log(error)
            res.status(400).json(error)
        })
});


function searchWikipedia(searchString) {
    return wiki().search(searchString)
        .then(searchPage => (searchPage.results))
}

/**
 * Search wikipedia for images and data
 */
router.get("/wikipedia/search/:searchString", (req, res) => {

    let searchString = req.params.searchString
    if (!!searchString) {
        return searchWikipedia(searchString)
            .then(search => (res.json(search)))
    }
    return res.json([])
});

/**
 * Lookup page wikipedia for images and data
 */
router.get("/wikipedia/page/:pageName", (req, res) => {
    let pageName = req.params.pageName
    return wiki().page(pageName)
        .then(page => {
            return Promise.all([page.mainImage(), page.images(), page.summary()])
                .then(
                values => (
                    res.json({
                        searchresult: false,
                        pageResult: {
                            name: page.raw.title || pageName,
                            mainImage: values[0],
                            images: values[1],
                            description: values[2],
                        }
                    }
                    )
                )
                )
                .catch((error) => {
                    console.log(error)
                    res.status(400).json(error + "")
                }
                )
        })
        .catch(err => searchWikipedia(pageName)
            .then(searchResult => res.json({ searchResult: searchResult, pageResult: false }))
        );
})

// configuring Multer to use files directory for storing files
// this is important because later we'll need to access file path
const storage = multer.diskStorage({
    destination: './files',
    filename(req, file, cb) {
        cb(null, `${new Date()}-${file.originalname}`);
    },
});
const upload = multer({ storage });
router.post("/upload", upload.single('file'), (req, res) => saveFile({ req, res }))


function saveFile({ req, res }) {
    const { filename, encoding, mimetype, originalname } = req.file
    const file = {
        _userId: req.userId,
        originalFilename: originalname,
        filename: filename,
        encoding: encoding,
        mimetype: mimetype,
        url: `localhost:8080/img/${filename}`
    }
    return File(file)
        .save()
        .then(result => res.json(result))
        .catch(err => {
            console.log("error: " + err)
            res.status(500).json(err.error)
        })
}


export default router;

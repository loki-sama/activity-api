import express from "express";
const router = express.Router();
import Tag from '../models/children/Tag'

import parseErrors from "../utils/parseErrors";
import authenticate from "../middlewares/authenticate";
import tools from '../utils/tools'
//router.use(authenticate);


/**
 * Get all tags
 */
router.get("/", (req, res) => {
    return Category.find({})
        .then((result) => {
            return res.json(tools.arrayToHashMap(result))
        })
        .catch((error) => {
            console.log(error)
            res.status(400).json(error)
        })
});

/**
 * Add a new tag to mongo
 */
router.get("/add", (req, res) => {
    tag = new Tag({tag: req.body.tag})
    
    return tag.save()
        .then((result) => {
            return res.json(result)
        })
        .catch((error) => {
            console.log(error)
            res.status(400).json(error)
        })
});

export default router;
import express from "express";
import jwt from "jsonwebtoken";
import Activities from '../models/Activities'
import Rank from '../models/children/Rank'
import Feed from '../models/children/Feed'
import Tag from '../models/children/Tag'

import wiki from 'wikijs';

const router = express.Router();
import parseErrors from "../utils/parseErrors";
import tools from "../utils/tools";

import authenticate from "../middlewares/authenticate";

router.use(authenticate);

/**
 * Get activities 
 */
router.get("/byid/:id", (req, res) => getActivityById(req, res));

function getActivityById(req, res) {
  Activities.findById(req.params.id)
    .populate({
      path: 'ranks',
      match: { _userId: req.userId }
    })
    .populate('feeds')
    .then(activity => Feed
      .populate(activity.feeds, { path: 'feeds.resource', model: 'File' })
      .then(populatedActivity => {
        return res.json(activity)
      })
    )
    .catch(err => res.status(400).json({ error: "activity does not exist" }))
}
/**
 * Get activities 
 */
router.post("/", (req, res) => getRandomActivity(req, res));

/**
 * Gets a random amount of acivities
 * @param {*} req 
 * @param {*} res 
 */
function getRandomActivity(req, res) {
  Activities.getRandom(req.body.amount, req.body.category, req.userId)
    .then((result) => (res.json(tools.arrayToHashMap(result))))
    .catch(err => (res.status(400).json(err + "")))
}
/**
 * Rank activity
 */
router.post("/userproperty/:id", (req, res) => Rank.updateUserActivityProperty(req.params.id, req.userId, req.body.ranks)
  .then(result => {
    if ((result.errors != undefined)) {
      return res.status(400).json(result)
    }
    return res.json(result)
  }));

/**
 * Add or update rank in mongo db
 * @param {*} req 
 * @param {*} res 
 */
function addUserProperty(req, res) {

  const _activityId = req.params.id
  if (!!_activityId) return res.status(400).json({ errors: "No activity id" })
  const validKeyMap = {
    rank: req.body.ranks.rank,
    seen: { $inc: 1 },
    favorite: req.body.ranks.favorite,
    clicked: { $inc: 1 },
    displayed: { $inc: 1 },
  }
  const updateQuery = Object.keys(req.body.ranks || {}).reduce(
    (output, key) => {
      if (!!validKeyMap[key]) output[key] = validKeyMap[key]
      return output
    }, {}
  )

  const data = { _userId: req.userId, _activityId: req.body.activityId, rank: req.body.rank }
  let query = { _userId: req.userId, _activityId: req.body.activityId }
  let options = { upsert: false, new: true, setDefaultsOnInsert: true, runValidators: true };
  return Rank.findOneAndUpdate(query, updateQuery, options)
    .then(rankRecord => {
      if (rankRecord === null) {
        rankRecord = new Rank(Object.assign({}, query, updateQuery))
        rankRecord.save()
      }
      return res.json({ rankRecord });
    })
    .catch(
    err => {
      return res.status(400).json({ errors: parseErrors(err.MongoError) })
    }
    );
}



/**
 * Add activity
 */
router.post("/add", (req, res) => addActivity(req, res));

/**
 * Adds a activity to db if imput is right
 * @param {*} req 
 * @param {*} res 
 */
function addActivity(req, res) {
  const tags = req.body.tags ? req.body.tags.split(' ') : []

  console.log('====================================');
  console.log(tags);
  console.log('====================================');
  delete req.body.tags
  const activity = new Activities(req.body)
  return activity.save()
    .then(activityRecord => {
      console.log(activityRecord)

      return Promise
        .all(tags.map(tagName => Tag.findOneAndUpdate({ tag: tagName }, { tag: tagName }, { upsert: true, new: true, setDefaultsOnInsert: true, runValidators: true })))
        .then(values =>
          Activities.findByIdAndUpdate(activityRecord._id, { $pushAll: { tags: values.map(value => value._id) } })
            .then(() => res.json(activityRecord))
        )
    })
    .catch(
    err => {
      console.log('====================================');
      console.log(err);
      console.log('====================================');
      return res.status(400).json({ errors: parseErrors(err.MongoError) })
    }
    );
}

router.get("/wikipedia", (req, res) => {
  let name = req.body
  wiki().page(name)
    .then(page => {
      Promise.all([page.mainImage(), page.images(), page.summary()]).then(
        values => {
          return res.json({
            name: name,
            mainImage: values[0],
            images: values[1],
            description: values[2],
          })
        }
      ).catch(error => {
        console.log(error)
        return res.json(error + "")
      }
        )
    }
    )
    .catch(error => {
      console.log(error)
      return res.json(error + "")
    }
    )
}
)

export default router;


/**
 * Get favorite by id
 */
router.get("/favorite/:id", (req, res) => {
  const _activityId = req.params.id

  //todo
})

/**
 * Post a new favorite or change
 */
router.post("/favorite", (req, res) => {
  req.userId
  const _userId = req.userId
  //todo
})

/**
 * Get feed by id
 */
router.get("/feed", (req, res) => {
  const _activityId = req.params.id
  const _userId = req.userId

  //todo
})

/**
 * Post a new feed
 */
router.post("/feed/:id", (req, res) => {
  const _activityId = req.params.id
  const _userId = req.userId
  console.log('====================================');

  console.log(req.body);

  console.log('====================================');
  return Feed
    .addFeed(_activityId, _userId, req.body)
    .then(result => res.json(result))
    .catch(err => res.status(400).json(err))
})  
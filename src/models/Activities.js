import mongoose from "mongoose";
import bcrypt from "bcrypt";
import uniqueValidator from "mongoose-unique-validator";
import faker from 'faker'
import Tag from './children/Tag'
import Rank from './children/Rank'
import Feed from './children/Feed'
import Category from './children/Category'


const schema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            index: true,
            unique: true
        },
        category: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Category',
        },
        description: {
            type: String,
            required: true,
        },
        image: {
            type: String,
            required: true,
        },
        videos: {
            type: String,
        },
        rank: {
            type: Number,
            default: 0,
        },
        tags: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tag',
            index: true,
        }],
        feeds: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Feed',
        }],
        ranks: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Rank',
        }],
        childActivities: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Activities',
        }],
        parentActivities: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Activities',
        }],
        wikipedia: {
            type: String,
        },
    },
    { timestamps: true },
);

schema.methods.createFake = function createFake() {
    this.name = faker.name.title()
    this.category = faker.name.category
    this.description = faker.lorem.text(100)
    this.image = faker.image.sports()
    this.tags = faker.lorem.text(20)
};

schema.methods.toJson = function toJson() {
    return {
        id: this.id,
        name: this.name,
        rank: this.rank,
        description: this.description,
        image: this.image
    };
};

schema.statics.getRandom = function getRandom(amount, categoryId = null, userId) {
    let match = {}
    if (!!categoryId) {
        match = {
            category: categoryId instanceof mongoose.Types.ObjectId ? categoryId : mongoose.Schema.ObjectId(categoryId)
        }
    }
    return this
        .aggregate()
        .match(match)
        .lookup({
            "from": "ranks",
            "localField": "ranks",
            "foreignField": "_id",
            "as": "ranks"
        })
        .sample(amount)
        .then(
        result => {
            if (typeof result === 'undefined' || !result.length) {
                return this
                    .aggregate()
                    .match({})
                    .lookup({
                        "from": "ranks",
                        "localField": "ranks",
                        "foreignField": "_id",
                        "as": "ranks"
                    })
                    .sample(amount)

            }
            return result
        }
        )
};

export default mongoose.model("Activities", schema);

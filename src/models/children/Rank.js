import mongoose from "mongoose";
import User from '../User'
import Activities from "../Activities";

const schema = new mongoose.Schema(
    {
        _userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
            index: true,
            //   unique: true
        },
        _activityId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Activities',
            required: true,
            index: true,
            //   unique: true
        },
        rank: {
            type: Number,
            min: 0,
            max: 2,
            default: 0,
            required: true,
            index: true,
        },
        favorite: {
            type: Number,
            min: 0,
            max: 1,
            default: 0,
            required: true,
            index: true,
        },
        //the item was nexted
        seen: {
            type: Number,
            min: 0,
            default: 0,
            required: true,
            index: true,
        },
        //the item was clicked
        clicked: {
            type: Number,
            min: 0,
            default: 0,
            required: true,
            index: true,
        },
        //the item was displayed in grid
        displayed: {
            type: Number,
            min: 0,
            default: 0,
            required: true,
            index: true,
        },
    },
    { timestamps: true },
)

schema.index({ _userId: 1, _activityId: 1 }, { unique: true });
schema.post('save', function (doc, next) {
    User.findById(this._userId, (err, user) => {
        if (!user) {
            return next(new Error('User doesn\'t exist!'));
        }
        user.ranks.push(this._id);
        user.save().catch(error => (console.log(err + '')));
        return next()
    })

    Activities.findById(this._activityId, (err, activity) => {
        if (!activity) {
            return next(new Error('Activity doesn\'t exist!'));
        }
        activity.ranks.push(this._id);
        activity.save().catch(error => (console.log(err + '')));;
        return next()
    })
});

schema.statics.updateUserActivityProperty = function (_activityId, _userId, data) {
    if (!_activityId) return { errors: "No activity id" }
    const validKeyMap = {
        rank: data.rank || null,
        seen: { $inc: 1 },
        favorite: data.favorite || null,
        clicked: { $inc: 1 },
        displayed: { $inc: 1 },
    }
    const updateQuery = Object.keys(data || {}).reduce(
        (output, key) => {
            if (!!validKeyMap[key]) output[key] = validKeyMap[key]
            return output
        }, {}
    )

    const query = { _userId: _userId, _activityId: _activityId }
    const options = { upsert: false, new: true, setDefaultsOnInsert: true, runValidators: true };
    return this.findOneAndUpdate(query, updateQuery, options)
        .then(rankRecord => {
            if (rankRecord === null) {
                rankRecord = new Rank(Object.assign({}, query, updateQuery))
                rankRecord.save()
            }
            return { rankRecord };
        })
        .catch(
        err => {
            console.log(err);

            return { errors: err.MongoError }
        }
        );
};
let Rank = mongoose.model("Rank", schema);
export default Rank

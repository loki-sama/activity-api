import mongoose from "mongoose";

const schema = new mongoose.Schema(
    {
        tag: {
            type: String,
            index: true,
            unique: true
        },
    }
);

let Tags = mongoose.model("Tag", schema);
export default Tags
import mongoose from "mongoose";
import User from '../User'
import Activities from "../Activities";

const schema = new mongoose.Schema(
    {
        _userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
            index: true,
            //   unique: true
        },
        filename: {
            type: String,
            required: true,
            index: true,
        },
        originalFilename: {
            type: String,
            required: true,
            index: true,
        },
        url: {
            type: String,
            required: true,
            index: true,
        },
        hight: {
            type: Number,
        },
        length: {
            type: Number,
        },
        size: {
            type: Number,
        },
        mimetype: {
            type: String
        },
        encoding: {
            type: String
        }
    },
    { timestamps: true },

)

let File = mongoose.model("File", schema);
export default File
import mongoose from "mongoose";

const schema = new mongoose.Schema(
    {
        category: {
            type: String,
            index: true,
            unique: true
        },
        description: {
            type: String,
            index: true,
            default: ""
        },
    }
)
export default mongoose.model("Category", schema);


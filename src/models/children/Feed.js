import mongoose from "mongoose";
import User from '../User'
import Activities from "../Activities";

const schema = new mongoose.Schema(
    {
        _userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
            index: true,
            //   unique: true
        },
        _activityId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Activities',
            required: true,
            index: true,
            //   unique: true
        },
        message: {
            type: String,
            required: true,
            index: true,
        },
        resource: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'File',
        }],
        kind: {
            type: String,
            index: true,
        },
    },
    { timestamps: true },

)

schema.post('save', function (doc, next) {
    User.findById(this._userId, (err, user) => {
        if (!user) {
            return next(new Error('User doesn\'t exist!'));
        }
        user.feeds.push(this._id);
        user.save()
        return next()
    })

    Activities.findById(this._activityId, (err, activity) => {
        if (!activity) {
            return next(new Error('Activity doesn\'t exist!'));
        }
        activity.feeds.push(this._id);
        activity.save().catch(error => (console.log(err + '')));
        return next()
    })
});

schema.statics.addFeed = function (_activityId, _userId, data) {

    if (!_activityId) return { errors: "No activity id" }
    const validKeyMap = {
        message: data.message || null,
        resource: data.resource || null,
        kind: data.kind || null,
        
    }
    const updateQuery = Object.keys(data || {})
        .reduce((output, key) => {
            if (!!validKeyMap[key]) output[key] = validKeyMap[key]
            return output
        }, {})

    const query = { _userId: _userId, _activityId: _activityId }
 
    let feedRecord = new Feed(Object.assign({}, query, updateQuery))

    return feedRecord.save();
};

let Feed = mongoose.model("Feed", schema);
export default Feed
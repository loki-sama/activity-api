import jwt from "jsonwebtoken";

export default (req, res, next) => {
  const header = req.headers.authorization;
  let token;

  if (header) token = header.split(" ")[1];

  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        res.status(401).json({ errors: { global: "Invalid token" } });
      } else {
        console.log('====================================');
        console.log(decoded);
        console.log('====================================');
        req.userEmail = decoded.email;
        req.userId = decoded._userId
        next();
      }
    });
  } else {
    res.status(401).json({ errors: { global: "No token" } });
  }
};
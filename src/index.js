import express from "express";
import path from "path";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import Promise from "bluebird";
import cors from "cors";

import playground from "./utils/playground";

import image from "./routes/image";
import auth from "./routes/auth";
import users from "./routes/users";
import activities from "./routes/activities";
import categories from "./routes/categories";
import tags from "./routes/tags";

var compression = require('compression')

dotenv.config();
const app = express();

app.use(compression())
app.use(bodyParser.json());
app.use(cors());

mongoose.Promise = Promise;
mongoose.connect(process.env.MONGODB_URL, { useMongoClient: true });

app.use("/api/auth", auth);
app.use("/api/users", users);
app.use("/api/activities", activities);
app.use("/api/images", image);
app.use("/api/categories", categories);
app.use("/api/tags", tags);

app.use("/api/playground", playground);
app.get("/img/:file", (req, res) => {
  res.sendFile(path.join(__dirname + '/../files/', req.params.file));
});
app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.listen(8080, () => console.log("Running on localhost:8080"));

//https://www.mediawiki.org/wiki/API:Main_page
//https://www.programmableweb.com/api/wikipedia


//api.php?action=query&list=search&srsearch=Albert%20Einstein&utf8=
import axios from "axios";
import Activities from '../models/Activities'
import mongoose from "mongoose";
//https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=Albert%20Einstein&utf8=&format=json
function scheduleRequests(axiosInstance, intervalMs) {
    let lastInvocationTime = undefined;

    const scheduler = (config) => {
        const now = Date.now();
        if (lastInvocationTime) {
            lastInvocationTime += intervalMs;
            const waitPeriodForThisRequest = lastInvocationTime - now;
            if (waitPeriodForThisRequest > 0) {
                return new Promise((resolve) => {
                    setTimeout(
                        () => resolve(config),
                        waitPeriodForThisRequest);
                });
            }
        }

        lastInvocationTime = now;
        return config;
    }

    axiosInstance.interceptors.request.use(scheduler);
}
const wikipediaService = axios.create({ baseURL: 'https://en.wikipedia.org/w/api.php' });
scheduleRequests(wikipediaService, 300);

const userAgent = { 'Api-User-Agent': 'ActivityMatch' }
export default {
    search: function (searchString) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                list: 'search',
                srsearch: searchString,
                utf8: 1,
                format: 'json'
            },
            headers: userAgent
        }
        )
    },

    getImage: function (title) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'images',
                titles: title,
                utf8: 1,
                format: 'json'
            },
            headers: userAgent
        }
        )
    },
    //action=query&prop=pageimages&format=json&piprop=name&titles=Able_seaman
    getTitleImage: function (title) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'pageimages',
                piprop: 'name',
                titles: title,
                utf8: 1,
                format: 'json'
            },
            headers: userAgent
        }
        )
    },

    getImageInfo: function (title) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'imageinfo',
                titles: title,
                utf8: 1,
                format: 'json',
                iiprop: 'extmetadata|url'
            },
            headers: userAgent
        }
        )
    },

    getImagesAndInfo: function (imagetitle) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'imageinfo',
                titles: imagetitle,
                utf8: 1,
                format: 'json',
                iiprop: 'extmetadata|url',
                generator: 'images'
            },
            headers: userAgent
        }
        )
    },
    getDescription: function (title) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'extracts',
                exintro: 1,
                explaintext: 1,
                titles: title,
                utf8: 1,
                format: 'json'
            },
            headers: userAgent
        }
        )
    },
    getWikipediaToInfo(pageTitels, categoryId = '59e07913b74d483ef01b21c9') {
        let title = pageTitels.shift()
        return axios.all(
            [this.getTitleImage(title),
            this.getImagesAndInfo(title),
            this.getDescription(title)]
        ).then(
            axios.spread(
                function (getTitleResult, getImagesResult, getDescriptionResult) {
                    //has title image
                    let key = Object.keys(getTitleResult.data.query.pages)[0]
                    if (!!getTitleResult.data.query.pages[key].pageimage) {
                        let titleImage = getTitleResult.data.query.pages[key].pageimage.replace(/ /g, "_")
                        let titleImageUrl = ""
                        let UsageTerms = ""

                        for (let x in getImagesResult.data.query.pages) {
                            let searchImage = getImagesResult.data.query.pages[x].title.replace(/ /g, "_")

                            if (searchImage == 'File:' + titleImage) {
                                titleImageUrl = getImagesResult.data.query.pages[x].imageinfo[0].url
                                // UsageTerms = getImagesResult.data.query.pages[x].imageinfo[0].extmetadata.UsageTerms.value
                                break;
                            }
                        }

                        let key2 = Object.keys(getDescriptionResult.data.query.pages)
                        return {
                            name: title,                            
                            titleImageUrl: titleImageUrl,
                            allImages: getImagesResult.data.query.pages,
                            getDescriptionResult: getDescriptionResult.data.query.pages[key2].extract
                        }

                    }
                    return { }
                }
            )
            )
    }


    //action=query&prop=imageinfo&iiprop=url|extmetadata&generator=images&titles=Able%20seaman



}
//images + licens
//https://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=url|extmetadata&generator=images&titles=Able%20seaman


//image url 
//https://commons.wikimedia.org/wiki/File:Able_seaman_preservation.JPG

//search 
//https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=Albert%20Einstein&utf8=&format=json
//Only title image 
//https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=name&titles=Able_seaman
//All images from title
//https://en.wikipedia.org/w/api.php?action=query&titles=Able_seaman&prop=images&format=json

//get discription
//https://en.wikipedia.org/w/api.php?action=query&prop=extracts&exintro=&explaintext=&titles=Stack_Overflow

//extra image info including licens
//https://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=extmetadata&titles=File%3aBrad_Pitt_at_Incirlik2.jpg
var request = new XMLHttpRequest();
request.open('GET', '/bar/foo.txt', false);  // `false` makes the request synchronous
request.send(null);

//https://www.mediawiki.org/wiki/API:Main_page
//https://www.programmableweb.com/api/wikipedia


//api.php?action=query&list=search&srsearch=Albert%20Einstein&utf8=

//https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=Albert%20Einstein&utf8=&format=json


const userAgent = { 'Api-User-Agent': 'ActivityMatch' }
export default {
    search: function (searchString) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                list: 'search',
                srsearch: searchString,
                utf8: 1,
                format: 'json'
            },
            headers: userAgent
        }
        )
    },

    getImage: function (title) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'images',
                titles: title,
                utf8: 1,
                format: 'json'
            },
            headers: userAgent
        }
        )
    },

    getTitleImage: function (title) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'pageimages',
                piprop: 'name',
                titles: title,
                utf8: 1,
                format: 'json'
            },
            headers: userAgent
        }
        )
    },

    getImageInfo: function (title) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'imageinfo',
                titles: title,
                utf8: 1,
                format: 'json',
                iiprop: 'extmetadata|url'
            },
            headers: userAgent
        }
        )
    },

    getImagesAndInfo: function (imagetitle) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'imageinfo',
                titles: imagetitle,
                utf8: 1,
                format: 'json',
                iiprop: 'extmetadata|url',
                generator: 'images'
            },
            headers: userAgent
        }
        )
    },
    getDescription: function (title) {
        return wikipediaService.get('', {
            params: {
                action: 'query',
                prop: 'extracts',
                exintro: 1,
                explaintext: 1,
                titles: title,
                utf8: 1,
                format: 'json'
            },
            headers: userAgent
        }
        )
    },
    //action=query&prop=imageinfo&iiprop=url|extmetadata&generator=images&titles=Able%20seaman



}
//images + licens
//https://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=url|extmetadata&generator=images&titles=Able%20seaman


//image url 
//https://commons.wikimedia.org/wiki/File:Able_seaman_preservation.JPG

//search 
//https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=Albert%20Einstein&utf8=&format=json
//Only title image 
//https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=name&titles=Able_seaman
//All images from title
//https://en.wikipedia.org/w/api.php?action=query&titles=Able_seaman&prop=images&format=json

//get discription
//https://en.wikipedia.org/w/api.php?action=query&prop=extracts&exintro=&explaintext=&titles=Stack_Overflow

//extra image info including licens
//https://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=extmetadata&titles=File%3aBrad_Pitt_at_Incirlik2.jpg
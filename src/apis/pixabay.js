import axios from "axios";


export function search(searchString) {
    searchString = searchString.replace(' ', '+').replace('%20', '+')
    return axios.get('https://pixabay.com/api/', {
        params: {
            key: process.env.PIXABAY_API_KEY,
            q: searchString, //searchstring
            lang: 'en', //language to search in ('de')
            image_type: 'photo',
            category: '', //category to search
            orientation: 'all', //orientation "all", "horizontal", "vertical" 
            per_page: 20 //how many images to find
        }
    })
}

